"use strict";
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _RedisCache_emitter, _RedisCache_cacheWorker, _RedisCache_cacheWorkerIsRunning, _RedisCache_chunkCollectorIntervalMs, _RedisCache_chunkSize, _RedisCache_redisInstance, _RedisCache_definedTables, _RedisCache_clientOptions, _RedisCache_saveHook, _RedisCache_getUniqueHash, _RedisCache_worker;
Object.defineProperty(exports, "__esModule", { value: true });
exports.RedisCache = void 0;
const crypto_1 = __importDefault(require("crypto"));
const events_1 = require("events");
class RedisCache {
    constructor(options) {
        _RedisCache_emitter.set(this, void 0);
        _RedisCache_cacheWorker.set(this, void 0);
        _RedisCache_cacheWorkerIsRunning.set(this, void 0);
        _RedisCache_chunkCollectorIntervalMs.set(this, void 0);
        _RedisCache_chunkSize.set(this, void 0);
        _RedisCache_redisInstance.set(this, void 0);
        _RedisCache_definedTables.set(this, void 0);
        _RedisCache_clientOptions.set(this, void 0);
        _RedisCache_saveHook.set(this, void 0);
        _RedisCache_getUniqueHash.set(this, async (table) => {
            const randomInput = Math.random() * 9e9;
            const hash = crypto_1.default
                .createHash('md5')
                .update(randomInput.toString())
                .digest('hex');
            const exists = await __classPrivateFieldGet(this, _RedisCache_redisInstance, "f").get(`${table}-${hash}`);
            return exists ? await __classPrivateFieldGet(this, _RedisCache_getUniqueHash, "f").call(this, table) : hash;
        });
        _RedisCache_worker.set(this, () => {
            return setInterval(async () => {
                const tableChunks = [];
                for await (const table of __classPrivateFieldGet(this, _RedisCache_definedTables, "f").values()) {
                    const chunks = await __classPrivateFieldGet(this, _RedisCache_redisInstance, "f").keys(`${table}-*`);
                    tableChunks.push(chunks.length);
                    for await (const chunkNamespace of chunks) {
                        const [_name, timestamp, _hash] = chunkNamespace.split('-');
                        const timeRatio = Number(timestamp) / Date.now();
                        const chunkElementsCount = await __classPrivateFieldGet(this, _RedisCache_redisInstance, "f").llen(chunkNamespace);
                        if (chunkElementsCount >= __classPrivateFieldGet(this, _RedisCache_chunkSize, "f") ||
                            timeRatio >= .65) {
                            const stringRows = await __classPrivateFieldGet(this, _RedisCache_redisInstance, "f").lrange(chunkNamespace, 0, -1);
                            const rows = stringRows.map(row => JSON.parse(row));
                            await __classPrivateFieldGet(this, _RedisCache_saveHook, "f").call(this, table, rows);
                            await __classPrivateFieldGet(this, _RedisCache_redisInstance, "f").del(chunkNamespace);
                        }
                    }
                }
                if (!tableChunks.filter(l => l > 0).length) {
                    __classPrivateFieldGet(this, _RedisCache_emitter, "f").emit('drop_worker');
                }
            }, __classPrivateFieldGet(this, _RedisCache_chunkCollectorIntervalMs, "f"));
        });
        __classPrivateFieldSet(this, _RedisCache_emitter, new events_1.EventEmitter(), "f");
        __classPrivateFieldSet(this, _RedisCache_definedTables, new Set([]), "f");
        __classPrivateFieldSet(this, _RedisCache_chunkSize, options.chunkSize, "f");
        __classPrivateFieldSet(this, _RedisCache_chunkCollectorIntervalMs, options.chunkCollectorIntervalMs, "f");
        __classPrivateFieldSet(this, _RedisCache_cacheWorker, null, "f");
        __classPrivateFieldSet(this, _RedisCache_cacheWorkerIsRunning, false, "f");
        __classPrivateFieldSet(this, _RedisCache_saveHook, () => {
            throw new Error('Save hook is not implemented!');
        }, "f");
        __classPrivateFieldGet(this, _RedisCache_emitter, "f").on('start_worker', () => {
            __classPrivateFieldSet(this, _RedisCache_cacheWorker, __classPrivateFieldGet(this, _RedisCache_worker, "f").call(this), "f");
            __classPrivateFieldSet(this, _RedisCache_cacheWorkerIsRunning, true, "f");
        });
        __classPrivateFieldGet(this, _RedisCache_emitter, "f").on('drop_worker', () => {
            clearInterval(__classPrivateFieldGet(this, _RedisCache_cacheWorker, "f"));
            __classPrivateFieldSet(this, _RedisCache_cacheWorker, null, "f");
            __classPrivateFieldSet(this, _RedisCache_cacheWorkerIsRunning, false, "f");
        });
    }
    healthpeak() {
        return true;
    }
    useSave(hook) {
        __classPrivateFieldSet(this, _RedisCache_saveHook, hook, "f");
    }
    useInstance({ instance, options }) {
        __classPrivateFieldSet(this, _RedisCache_clientOptions, options, "f");
        __classPrivateFieldSet(this, _RedisCache_redisInstance, instance, "f");
    }
    async cacheTableRows(table, rows) {
        if (!__classPrivateFieldGet(this, _RedisCache_redisInstance, "f")) {
            throw new Error('Redis Client is not initialized');
        }
        if (!__classPrivateFieldGet(this, _RedisCache_cacheWorkerIsRunning, "f")) {
            __classPrivateFieldGet(this, _RedisCache_emitter, "f").emit('start_worker');
        }
        if (!__classPrivateFieldGet(this, _RedisCache_definedTables, "f").has(table)) {
            __classPrivateFieldGet(this, _RedisCache_definedTables, "f").add(table);
        }
        const response = {
            inserted: 0,
            data: [],
            chunk: '',
            isNew: false
        };
        const stringifiedRows = rows.map(row => JSON.stringify(row));
        const chunks = await __classPrivateFieldGet(this, _RedisCache_redisInstance, "f").keys(`${table}-*`);
        if (!chunks.length) {
            const hash = await __classPrivateFieldGet(this, _RedisCache_getUniqueHash, "f").call(this, table);
            const timestamp = Date.now();
            const key = `${table}-${timestamp}-${hash}`;
            await __classPrivateFieldGet(this, _RedisCache_redisInstance, "f").rpush(key, ...stringifiedRows);
            await __classPrivateFieldGet(this, _RedisCache_redisInstance, "f").expire(key, Math.floor(__classPrivateFieldGet(this, _RedisCache_clientOptions, "f").ttlMs / 1000));
            response.data = [...rows];
            response.inserted = rows.length;
            response.chunk = key;
            response.isNew = true;
        }
        else {
            await __classPrivateFieldGet(this, _RedisCache_redisInstance, "f").rpush(chunks[0], ...stringifiedRows);
            response.data = [...rows];
            response.inserted = rows.length;
            response.chunk = chunks[0];
            response.isNew = false;
        }
        return response;
    }
}
exports.RedisCache = RedisCache;
_RedisCache_emitter = new WeakMap(), _RedisCache_cacheWorker = new WeakMap(), _RedisCache_cacheWorkerIsRunning = new WeakMap(), _RedisCache_chunkCollectorIntervalMs = new WeakMap(), _RedisCache_chunkSize = new WeakMap(), _RedisCache_redisInstance = new WeakMap(), _RedisCache_definedTables = new WeakMap(), _RedisCache_clientOptions = new WeakMap(), _RedisCache_saveHook = new WeakMap(), _RedisCache_getUniqueHash = new WeakMap(), _RedisCache_worker = new WeakMap();
