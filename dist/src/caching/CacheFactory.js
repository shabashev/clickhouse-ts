"use strict";
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var _CacheFactory_cacheType;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CacheFactory = void 0;
const RedisCache_1 = require("./RedisCache");
const SystemCache_1 = require("./SystemCache");
class CacheFactory {
    constructor(cacheType) {
        _CacheFactory_cacheType.set(this, void 0);
        __classPrivateFieldSet(this, _CacheFactory_cacheType, cacheType, "f");
    }
    getCacheClient({ chunkSize = 1000, chunkCollectorIntervalMs = 100000 }) {
        if (__classPrivateFieldGet(this, _CacheFactory_cacheType, "f") === 'system') {
            return new SystemCache_1.SystemCache({
                chunkSize,
                chunkCollectorIntervalMs
            });
        }
        if (__classPrivateFieldGet(this, _CacheFactory_cacheType, "f") === 'redis') {
            return new RedisCache_1.RedisCache({
                chunkSize,
                chunkCollectorIntervalMs
            });
        }
    }
}
exports.CacheFactory = CacheFactory;
_CacheFactory_cacheType = new WeakMap();
