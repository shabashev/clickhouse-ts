import { ClickhouseAppCache } from "./ClickhouseAppCache";
import { Redis } from 'ioredis';
export declare class RedisCache implements ClickhouseAppCache {
    #private;
    constructor(options: {
        chunkSize: number;
        chunkCollectorIntervalMs: number;
    });
    healthpeak(): boolean;
    useSave(hook: (table: string, rows: Record<any, any>[]) => Promise<void>): void;
    useInstance({ instance, options }: {
        instance: Redis;
        options: {
            ttlMs: number;
        };
    }): void;
    cacheTableRows(table: string, rows: Record<string, any>[]): Promise<{
        inserted: number;
        data: Record<string, any>[];
        chunk: string;
        isNew: boolean;
    }>;
}
