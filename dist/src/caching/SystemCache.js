"use strict";
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var _SystemCache_storage, _SystemCache_cacheWorker, _SystemCache_cacheWorkerIsRunning, _SystemCache_saveHook, _SystemCache_chunkCollectorIntervalMs, _SystemCache_chunkSize, _SystemCache_worker, _SystemCache_emitter;
Object.defineProperty(exports, "__esModule", { value: true });
exports.SystemCache = void 0;
const stream_1 = require("stream");
class SystemCache {
    constructor(options) {
        _SystemCache_storage.set(this, void 0);
        _SystemCache_cacheWorker.set(this, void 0);
        _SystemCache_cacheWorkerIsRunning.set(this, void 0);
        _SystemCache_saveHook.set(this, void 0);
        _SystemCache_chunkCollectorIntervalMs.set(this, void 0);
        _SystemCache_chunkSize.set(this, void 0);
        _SystemCache_worker.set(this, () => {
            return setInterval(async () => {
                const tableLengths = [];
                for await (const table of Object.keys(__classPrivateFieldGet(this, _SystemCache_storage, "f"))) {
                    const storageTableLengthSnapshot = __classPrivateFieldGet(this, _SystemCache_storage, "f")[table].length;
                    tableLengths.push(storageTableLengthSnapshot);
                    const rows = __classPrivateFieldGet(this, _SystemCache_storage, "f")[table].splice(0, storageTableLengthSnapshot);
                    if (rows.length) {
                        await __classPrivateFieldGet(this, _SystemCache_saveHook, "f").call(this, table, rows);
                    }
                }
                if (!tableLengths.filter(l => l > 0).length) {
                    __classPrivateFieldGet(this, _SystemCache_emitter, "f").emit('drop_worker');
                }
            }, __classPrivateFieldGet(this, _SystemCache_chunkCollectorIntervalMs, "f"));
        });
        _SystemCache_emitter.set(this, void 0);
        __classPrivateFieldSet(this, _SystemCache_emitter, new stream_1.EventEmitter(), "f");
        __classPrivateFieldSet(this, _SystemCache_storage, {}, "f");
        __classPrivateFieldSet(this, _SystemCache_saveHook, (_1, _2) => {
            throw new Error('Hook is not initialized');
        }, "f");
        __classPrivateFieldSet(this, _SystemCache_chunkSize, options.chunkSize, "f");
        __classPrivateFieldSet(this, _SystemCache_chunkCollectorIntervalMs, options.chunkCollectorIntervalMs, "f");
        __classPrivateFieldSet(this, _SystemCache_cacheWorker, null, "f");
        __classPrivateFieldSet(this, _SystemCache_cacheWorkerIsRunning, false, "f");
        __classPrivateFieldGet(this, _SystemCache_emitter, "f").on('start_worker', () => {
            __classPrivateFieldSet(this, _SystemCache_cacheWorker, __classPrivateFieldGet(this, _SystemCache_worker, "f").call(this), "f");
            __classPrivateFieldSet(this, _SystemCache_cacheWorkerIsRunning, true, "f");
        });
        __classPrivateFieldGet(this, _SystemCache_emitter, "f").on('drop_worker', () => {
            clearInterval(__classPrivateFieldGet(this, _SystemCache_cacheWorker, "f"));
            __classPrivateFieldSet(this, _SystemCache_cacheWorker, null, "f");
            __classPrivateFieldSet(this, _SystemCache_cacheWorkerIsRunning, false, "f");
        });
    }
    healthpeak() {
        return true;
    }
    // hooks
    useSave(hook) {
        __classPrivateFieldSet(this, _SystemCache_saveHook, hook, "f");
    }
    useInstance(_) {
        return;
    }
    async cacheTableRows(table, rows) {
        if (!__classPrivateFieldGet(this, _SystemCache_cacheWorkerIsRunning, "f")) {
            __classPrivateFieldGet(this, _SystemCache_emitter, "f").emit('start_worker');
        }
        if (!__classPrivateFieldGet(this, _SystemCache_storage, "f")[table]) {
            __classPrivateFieldGet(this, _SystemCache_storage, "f")[table] = [];
        }
        __classPrivateFieldGet(this, _SystemCache_storage, "f")[table].push(...rows);
        const storageTableLengthSnapshot = __classPrivateFieldGet(this, _SystemCache_storage, "f")[table].length;
        if (storageTableLengthSnapshot >= __classPrivateFieldGet(this, _SystemCache_chunkSize, "f")) {
            const rows = __classPrivateFieldGet(this, _SystemCache_storage, "f")[table].splice(0, storageTableLengthSnapshot);
            await __classPrivateFieldGet(this, _SystemCache_saveHook, "f").call(this, table, rows);
        }
        return {
            inserted: rows.length,
            data: rows,
            chunk: table,
            isNew: !!__classPrivateFieldGet(this, _SystemCache_storage, "f")[table].length
        };
    }
}
exports.SystemCache = SystemCache;
_SystemCache_storage = new WeakMap(), _SystemCache_cacheWorker = new WeakMap(), _SystemCache_cacheWorkerIsRunning = new WeakMap(), _SystemCache_saveHook = new WeakMap(), _SystemCache_chunkCollectorIntervalMs = new WeakMap(), _SystemCache_chunkSize = new WeakMap(), _SystemCache_worker = new WeakMap(), _SystemCache_emitter = new WeakMap();
