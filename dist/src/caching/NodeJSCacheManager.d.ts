/// <reference types="node" />
import { EventEmitter } from "stream";
export declare class NodeJSCacheManager extends EventEmitter {
    #private;
    constructor(options: {
        chunkTTLSeconds: number;
        chunkExpireTimeSeconds: number;
        chunkSizeLimit: number;
        chunkResolverIntervalSeconds: number;
        chunkResolveType: 'autoInsert' | 'events';
        useInsert: (table: string, rows: Record<string, any>[]) => Promise<void>;
    });
    cache(table: string, items: string[]): Promise<{
        cached: number;
        chunk: string;
    }>;
}
