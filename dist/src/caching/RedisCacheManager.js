"use strict";
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _RedisCacheManager_tableChunks, _RedisCacheManager_instance, _RedisCacheManager_chunkResolver, _RedisCacheManager_splitter, _RedisCacheManager_options, _RedisCacheManager_checkChunks, _RedisCacheManager_resolveChunk, _RedisCacheManager_createChunk, _RedisCacheManager_deleteChunk, _RedisCacheManager_getChunk;
Object.defineProperty(exports, "__esModule", { value: true });
exports.RedisCacheManager = void 0;
const dayjs_1 = __importDefault(require("dayjs"));
const stream_1 = require("stream");
const crypto_1 = __importDefault(require("crypto"));
const Debug_1 = require("../debug/Debug");
class RedisCacheManager extends stream_1.EventEmitter {
    constructor(options) {
        super();
        _RedisCacheManager_tableChunks.set(this, void 0);
        _RedisCacheManager_instance.set(this, void 0);
        _RedisCacheManager_chunkResolver.set(this, void 0);
        _RedisCacheManager_splitter.set(this, '-');
        _RedisCacheManager_options.set(this, void 0);
        _RedisCacheManager_checkChunks.set(this, async () => {
            this.checkInstance();
            Debug_1.debug.log('tableChunks_profiling', { tableChunks: __classPrivateFieldGet(this, _RedisCacheManager_tableChunks, "f") });
            for await (const [table, chunkIds] of Object.entries(__classPrivateFieldGet(this, _RedisCacheManager_tableChunks, "f"))) {
                for await (const chunkId of chunkIds) {
                    const chunkLen = await __classPrivateFieldGet(this, _RedisCacheManager_instance, "f").llen(chunkId);
                    Debug_1.debug.log(`chunk.${table}.${chunkId}`, { chunkLen });
                }
            }
            const now = dayjs_1.default();
            for await (const [table, chunkNamespaces] of Object.entries(__classPrivateFieldGet(this, _RedisCacheManager_tableChunks, "f"))) {
                for (const chunkNamespace of chunkNamespaces) {
                    Debug_1.debug.log('RedisCacheClientManager.#checkChunk', { table, chunkNamespace });
                    const [_chunk_, _table, _id, _strExpiresAtUnix] = chunkNamespace.split(__classPrivateFieldGet(this, _RedisCacheManager_splitter, "f"));
                    const expiresAt = Number(_strExpiresAtUnix);
                    const chunkNamespaceTTL = await __classPrivateFieldGet(this, _RedisCacheManager_instance, "f").ttl(chunkNamespace);
                    if (chunkNamespaceTTL < 0) {
                        await __classPrivateFieldGet(this, _RedisCacheManager_instance, "f").expireat(chunkNamespace, now.add(__classPrivateFieldGet(this, _RedisCacheManager_options, "f").chunkExpireTimeSeconds ?? 86400, 's').unix());
                    }
                    // const chunkFull = await this.#instance!.llen(chunkNamespace) >= this.#options.chunkSizeLimit
                    const chunkTooOld = now.unix() > expiresAt;
                    if (chunkTooOld) {
                        await __classPrivateFieldGet(this, _RedisCacheManager_resolveChunk, "f").call(this, chunkNamespace, table);
                    }
                }
            }
        });
        _RedisCacheManager_resolveChunk.set(this, async (chunkId, table) => {
            const raw = await __classPrivateFieldGet(this, _RedisCacheManager_instance, "f").lrange(chunkId, 0, -1);
            const rows = raw.map(str => JSON.parse(str));
            if (__classPrivateFieldGet(this, _RedisCacheManager_options, "f").chunkResolveType === 'autoInsert') {
                await __classPrivateFieldGet(this, _RedisCacheManager_options, "f").useInsert(table, rows);
            }
            else if (__classPrivateFieldGet(this, _RedisCacheManager_options, "f").chunkResolveType === 'events') {
                this.emit('chunk', chunkId, table, rows);
            }
            else {
                throw new Error('resolveType is not correct!');
            }
            await __classPrivateFieldGet(this, _RedisCacheManager_deleteChunk, "f").call(this, table, chunkId);
        });
        this.checkInstance = () => {
            if (!__classPrivateFieldGet(this, _RedisCacheManager_instance, "f")) {
                throw new Error('Redis instance is not implemented!');
            }
        };
        _RedisCacheManager_createChunk.set(this, (table) => {
            const now = dayjs_1.default();
            if (!__classPrivateFieldGet(this, _RedisCacheManager_tableChunks, "f")[table]) {
                __classPrivateFieldGet(this, _RedisCacheManager_tableChunks, "f")[table] = [];
            }
            const id = crypto_1.default
                .createHash('md5')
                .update((Math.random() * 9e5).toString())
                .digest('hex');
            const ttl = now.add(__classPrivateFieldGet(this, _RedisCacheManager_options, "f").chunkTTLSeconds, 'second').unix();
            const newChunk = ['chunk', table, id, ttl].join(__classPrivateFieldGet(this, _RedisCacheManager_splitter, "f"));
            __classPrivateFieldGet(this, _RedisCacheManager_tableChunks, "f")[table].push(newChunk);
            Debug_1.debug.log('RedisCacheClientManager.#createChunk', { table, newChunk, tableChunks: __classPrivateFieldGet(this, _RedisCacheManager_tableChunks, "f") });
            return newChunk;
        });
        _RedisCacheManager_deleteChunk.set(this, async (table, chunk) => {
            if (!__classPrivateFieldGet(this, _RedisCacheManager_tableChunks, "f")[table]) {
                __classPrivateFieldGet(this, _RedisCacheManager_tableChunks, "f")[table] = [];
            }
            __classPrivateFieldGet(this, _RedisCacheManager_tableChunks, "f")[table] = __classPrivateFieldGet(this, _RedisCacheManager_tableChunks, "f")[table].filter(tableChunk => tableChunk !== chunk);
            await __classPrivateFieldGet(this, _RedisCacheManager_instance, "f").del(chunk);
            Debug_1.debug.log('RedisCacheClientManager.#deleteChunk', { table, chunk, tableChunks: __classPrivateFieldGet(this, _RedisCacheManager_tableChunks, "f") });
        });
        _RedisCacheManager_getChunk.set(this, (table) => {
            const now = dayjs_1.default();
            const hasChunk = Boolean(__classPrivateFieldGet(this, _RedisCacheManager_tableChunks, "f")[table]?.length);
            if (hasChunk) {
                const chunk = __classPrivateFieldGet(this, _RedisCacheManager_tableChunks, "f")[table].find(chunk => {
                    const [_chunk, _table, _id, strExpiresAtUnix] = chunk.split(__classPrivateFieldGet(this, _RedisCacheManager_splitter, "f"));
                    const expiresAt = Number(strExpiresAtUnix);
                    if (now.unix() < expiresAt) {
                        return true;
                    }
                });
                return chunk ?? __classPrivateFieldGet(this, _RedisCacheManager_createChunk, "f").call(this, table);
            }
            else {
                return __classPrivateFieldGet(this, _RedisCacheManager_createChunk, "f").call(this, table);
            }
        });
        if (options.chunkExpireTimeSeconds <= options.chunkTTLSeconds) {
            throw new Error('chunkExpireTimeSeconds must be greater then chunkTTLSeconds');
        }
        __classPrivateFieldSet(this, _RedisCacheManager_tableChunks, {}, "f");
        __classPrivateFieldSet(this, _RedisCacheManager_options, options, "f");
    }
    async useRedisInstance(instance) {
        if (__classPrivateFieldGet(this, _RedisCacheManager_instance, "f")) {
            return;
        }
        const intervalTime = 1000 * __classPrivateFieldGet(this, _RedisCacheManager_options, "f").chunkResolverIntervalSeconds;
        __classPrivateFieldSet(this, _RedisCacheManager_instance, instance, "f");
        const cachedChunkTables = await __classPrivateFieldGet(this, _RedisCacheManager_instance, "f").keys(`chunk${__classPrivateFieldGet(this, _RedisCacheManager_splitter, "f")}*`);
        Debug_1.debug.log('RedisCacheClientManager.useRedisInstance', { cachedChunkTables });
        for await (const chunkTable of cachedChunkTables) {
            const [_, table] = chunkTable.split(__classPrivateFieldGet(this, _RedisCacheManager_splitter, "f"));
            if (!__classPrivateFieldGet(this, _RedisCacheManager_tableChunks, "f")[table]) {
                __classPrivateFieldGet(this, _RedisCacheManager_tableChunks, "f")[table] = [];
            }
            __classPrivateFieldGet(this, _RedisCacheManager_tableChunks, "f")[table].push(chunkTable);
        }
        __classPrivateFieldSet(this, _RedisCacheManager_chunkResolver, setInterval(async () => await __classPrivateFieldGet(this, _RedisCacheManager_checkChunks, "f").call(this), intervalTime), "f");
    }
    async cache(table, items) {
        this.checkInstance();
        let chunkCandidate = __classPrivateFieldGet(this, _RedisCacheManager_getChunk, "f").call(this, table);
        while (await __classPrivateFieldGet(this, _RedisCacheManager_instance, "f").llen(chunkCandidate) >= __classPrivateFieldGet(this, _RedisCacheManager_options, "f").chunkSizeLimit) {
            await __classPrivateFieldGet(this, _RedisCacheManager_resolveChunk, "f").call(this, chunkCandidate, table);
            chunkCandidate = __classPrivateFieldGet(this, _RedisCacheManager_getChunk, "f").call(this, table);
        }
        const cached = await __classPrivateFieldGet(this, _RedisCacheManager_instance, "f").rpush(chunkCandidate, ...items);
        Debug_1.debug.log('RedisCacheClientManager.cache', { cached, chunk: chunkCandidate });
        return {
            cached,
            chunk: chunkCandidate,
        };
    }
}
exports.RedisCacheManager = RedisCacheManager;
_RedisCacheManager_tableChunks = new WeakMap(), _RedisCacheManager_instance = new WeakMap(), _RedisCacheManager_chunkResolver = new WeakMap(), _RedisCacheManager_splitter = new WeakMap(), _RedisCacheManager_options = new WeakMap(), _RedisCacheManager_checkChunks = new WeakMap(), _RedisCacheManager_resolveChunk = new WeakMap(), _RedisCacheManager_createChunk = new WeakMap(), _RedisCacheManager_deleteChunk = new WeakMap(), _RedisCacheManager_getChunk = new WeakMap();
