import { ClickhouseAppCache } from "./ClickhouseAppCache";
export declare class SystemCache implements ClickhouseAppCache {
    #private;
    constructor(options: {
        chunkSize: number;
        chunkCollectorIntervalMs: number;
    });
    healthpeak(): boolean;
    useSave(hook: (table: string, rows: Record<any, any>[]) => Promise<void>): void;
    useInstance(_: any): void;
    cacheTableRows(table: string, rows: Record<string, any>[]): Promise<{
        inserted: number;
        data: Record<string, any>[];
        chunk: string;
        isNew: boolean;
    }>;
}
