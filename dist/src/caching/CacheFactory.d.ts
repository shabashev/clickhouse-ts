import { RedisCache } from "./RedisCache";
import { SystemCache } from "./SystemCache";
export declare class CacheFactory {
    #private;
    constructor(cacheType: 'system' | 'redis' | undefined);
    getCacheClient({ chunkSize, chunkCollectorIntervalMs }: {
        chunkSize?: number;
        chunkCollectorIntervalMs?: number;
    }): SystemCache | RedisCache | undefined;
}
