"use strict";
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _NodeJSCacheManager_tableChunks, _NodeJSCacheManager_chunkResolver, _NodeJSCacheManager_splitter, _NodeJSCacheManager_options, _NodeJSCacheManager_checkChunks, _NodeJSCacheManager_resolveChunk, _NodeJSCacheManager_createChunk, _NodeJSCacheManager_deleteChunk, _NodeJSCacheManager_getChunk;
Object.defineProperty(exports, "__esModule", { value: true });
exports.NodeJSCacheManager = void 0;
const dayjs_1 = __importDefault(require("dayjs"));
const stream_1 = require("stream");
const crypto_1 = __importDefault(require("crypto"));
const Debug_1 = require("../debug/Debug");
class NodeJSCacheManager extends stream_1.EventEmitter {
    constructor(options) {
        super();
        _NodeJSCacheManager_tableChunks.set(this, void 0);
        _NodeJSCacheManager_chunkResolver.set(this, void 0);
        _NodeJSCacheManager_splitter.set(this, '-');
        _NodeJSCacheManager_options.set(this, void 0);
        _NodeJSCacheManager_checkChunks.set(this, async () => {
            Debug_1.debug.log('tableChunks_profiling', { tableChunks: __classPrivateFieldGet(this, _NodeJSCacheManager_tableChunks, "f") });
            for await (const [table, chunkIds] of Object.entries(__classPrivateFieldGet(this, _NodeJSCacheManager_tableChunks, "f"))) {
                for await (const chunkId of Object.keys(chunkIds)) {
                    const chunkLen = __classPrivateFieldGet(this, _NodeJSCacheManager_tableChunks, "f")[table][chunkId].length;
                    Debug_1.debug.log(`chunk.${table}.${chunkId}`, { chunkLen });
                }
            }
            const now = dayjs_1.default();
            for await (const [table, chunkNamespaces] of Object.entries(__classPrivateFieldGet(this, _NodeJSCacheManager_tableChunks, "f"))) {
                for (const chunkNamespace of Object.keys(chunkNamespaces)) {
                    Debug_1.debug.log('RedisCacheClientManager.#checkChunk', { table, chunkNamespace });
                    const [_chunk_, _table, _id, _strExpiresAtUnix] = chunkNamespace.split(__classPrivateFieldGet(this, _NodeJSCacheManager_splitter, "f"));
                    const expiresAt = Number(_strExpiresAtUnix);
                    // const chunkNamespaceTTL = await this.#instance!.ttl(chunkNamespace)
                    // if (chunkNamespaceTTL < 0) {
                    //   await this.#instance!.expireat(
                    //     chunkNamespace,
                    //     now.add(this.#options.chunkExpireTimeSeconds ?? 86400, 's').unix()
                    //   )
                    // }
                    // const chunkFull = await this.#instance!.llen(chunkNamespace) >= this.#options.chunkSizeLimit
                    const chunkTooOld = now.unix() > expiresAt;
                    if (chunkTooOld) {
                        await __classPrivateFieldGet(this, _NodeJSCacheManager_resolveChunk, "f").call(this, chunkNamespace, table);
                    }
                }
            }
        });
        _NodeJSCacheManager_resolveChunk.set(this, async (chunkId, table) => {
            const raw = __classPrivateFieldGet(this, _NodeJSCacheManager_tableChunks, "f")[table][chunkId];
            const rows = raw.map(str => JSON.parse(str));
            if (__classPrivateFieldGet(this, _NodeJSCacheManager_options, "f").chunkResolveType === 'autoInsert') {
                await __classPrivateFieldGet(this, _NodeJSCacheManager_options, "f").useInsert(table, rows);
            }
            else if (__classPrivateFieldGet(this, _NodeJSCacheManager_options, "f").chunkResolveType === 'events') {
                this.emit('chunk', chunkId, table, [...rows]);
            }
            else {
                throw new Error('resolveType is not correct!');
            }
            __classPrivateFieldGet(this, _NodeJSCacheManager_deleteChunk, "f").call(this, table, chunkId);
        });
        _NodeJSCacheManager_createChunk.set(this, (table) => {
            const now = dayjs_1.default();
            // if (!this.#tableChunks[table]) {
            //   this.#tableChunks[table] = []
            // }
            const id = crypto_1.default
                .createHash('md5')
                .update((Math.random() * 9e5).toString())
                .digest('hex');
            const ttl = now.add(__classPrivateFieldGet(this, _NodeJSCacheManager_options, "f").chunkTTLSeconds, 'second').unix();
            const newChunk = ['chunk', table, id, ttl].join(__classPrivateFieldGet(this, _NodeJSCacheManager_splitter, "f"));
            __classPrivateFieldGet(this, _NodeJSCacheManager_tableChunks, "f")[table][newChunk] = [];
            Debug_1.debug.log('RedisCacheClientManager.#createChunk', { table, newChunk, tableChunks: __classPrivateFieldGet(this, _NodeJSCacheManager_tableChunks, "f") });
            return newChunk;
        });
        _NodeJSCacheManager_deleteChunk.set(this, (table, chunk) => {
            // if (!this.#tableChunks[table]) {
            // this.#tableChunks[table] = []
            // }
            // this.#tableChunks[table] = this.#tableChunks[table].filter(tableChunk => tableChunk !== chunk)
            delete __classPrivateFieldGet(this, _NodeJSCacheManager_tableChunks, "f")[table][chunk];
            Debug_1.debug.log('RedisCacheClientManager.#deleteChunk', { table, chunk, tableChunks: __classPrivateFieldGet(this, _NodeJSCacheManager_tableChunks, "f") });
        });
        _NodeJSCacheManager_getChunk.set(this, (table) => {
            const now = dayjs_1.default();
            if (!__classPrivateFieldGet(this, _NodeJSCacheManager_tableChunks, "f")[table]) {
                __classPrivateFieldGet(this, _NodeJSCacheManager_tableChunks, "f")[table] = {};
            }
            const hasChunk = Boolean(Object.values(__classPrivateFieldGet(this, _NodeJSCacheManager_tableChunks, "f")[table]));
            if (hasChunk) {
                const chunk = Object.keys(__classPrivateFieldGet(this, _NodeJSCacheManager_tableChunks, "f")[table]).find(chunk => {
                    const [_chunk, _table, _id, strExpiresAtUnix] = chunk.split(__classPrivateFieldGet(this, _NodeJSCacheManager_splitter, "f"));
                    const expiresAt = Number(strExpiresAtUnix);
                    if (now.unix() < expiresAt) {
                        return true;
                    }
                });
                return chunk ?? __classPrivateFieldGet(this, _NodeJSCacheManager_createChunk, "f").call(this, table);
            }
            else {
                return __classPrivateFieldGet(this, _NodeJSCacheManager_createChunk, "f").call(this, table);
            }
        });
        if (options.chunkExpireTimeSeconds <= options.chunkTTLSeconds) {
            throw new Error('chunkExpireTimeSeconds must be greater then chunkTTLSeconds');
        }
        __classPrivateFieldSet(this, _NodeJSCacheManager_tableChunks, {}, "f");
        __classPrivateFieldSet(this, _NodeJSCacheManager_options, options, "f");
        const intervalTime = 1000 * __classPrivateFieldGet(this, _NodeJSCacheManager_options, "f").chunkResolverIntervalSeconds;
        __classPrivateFieldSet(this, _NodeJSCacheManager_chunkResolver, setInterval(async () => await __classPrivateFieldGet(this, _NodeJSCacheManager_checkChunks, "f").call(this), intervalTime), "f");
    }
    async cache(table, items) {
        let chunkCandidate = __classPrivateFieldGet(this, _NodeJSCacheManager_getChunk, "f").call(this, table);
        while (__classPrivateFieldGet(this, _NodeJSCacheManager_tableChunks, "f")[table][chunkCandidate].length >= __classPrivateFieldGet(this, _NodeJSCacheManager_options, "f").chunkSizeLimit) {
            await __classPrivateFieldGet(this, _NodeJSCacheManager_resolveChunk, "f").call(this, chunkCandidate, table);
            chunkCandidate = __classPrivateFieldGet(this, _NodeJSCacheManager_getChunk, "f").call(this, table);
        }
        __classPrivateFieldGet(this, _NodeJSCacheManager_tableChunks, "f")[table][chunkCandidate].push(...items);
        // const cached = await this.#instance!.rpush(chunkCandidate, ...items)
        Debug_1.debug.log('RedisCacheClientManager.cache', {
            cached: items.length,
            chunk: chunkCandidate
        });
        return {
            cached: items.length,
            chunk: chunkCandidate,
        };
    }
}
exports.NodeJSCacheManager = NodeJSCacheManager;
_NodeJSCacheManager_tableChunks = new WeakMap(), _NodeJSCacheManager_chunkResolver = new WeakMap(), _NodeJSCacheManager_splitter = new WeakMap(), _NodeJSCacheManager_options = new WeakMap(), _NodeJSCacheManager_checkChunks = new WeakMap(), _NodeJSCacheManager_resolveChunk = new WeakMap(), _NodeJSCacheManager_createChunk = new WeakMap(), _NodeJSCacheManager_deleteChunk = new WeakMap(), _NodeJSCacheManager_getChunk = new WeakMap();
