/// <reference types="node" />
import { Redis } from "ioredis";
import { EventEmitter } from "stream";
export declare class RedisCacheManager extends EventEmitter {
    #private;
    constructor(options: {
        chunkTTLSeconds: number;
        chunkExpireTimeSeconds: number;
        chunkSizeLimit: number;
        chunkResolverIntervalSeconds: number;
        chunkResolveType: 'autoInsert' | 'events';
        useInsert: (table: string, rows: Record<string, any>[]) => Promise<void>;
    });
    readonly checkInstance: () => void;
    useRedisInstance(instance: Redis): Promise<void>;
    cache(table: string, items: string[]): Promise<{
        cached: number;
        chunk: string;
    }>;
}
