"use strict";
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _Clickhouse_httpClient, _Clickhouse_options, _Clickhouse_cacheManager, _Clickhouse_onChunkCb, _Clickhouse_redisClient, _Clickhouse_isFirstInsert;
Object.defineProperty(exports, "__esModule", { value: true });
exports.Clickhouse = void 0;
const ClickhouseHttpClient_1 = require("../httpClient/ClickhouseHttpClient");
const sqlstring_1 = __importDefault(require("sqlstring"));
const RedisCacheManager_1 = require("../caching/RedisCacheManager");
const Debug_1 = require("../debug/Debug");
const NodeJSCacheManager_1 = require("../caching/NodeJSCacheManager");
class Clickhouse {
    constructor(context, options) {
        // DI
        _Clickhouse_httpClient.set(this, void 0);
        _Clickhouse_options.set(this, void 0);
        _Clickhouse_cacheManager.set(this, void 0);
        _Clickhouse_onChunkCb.set(this, void 0);
        _Clickhouse_redisClient.set(this, void 0);
        _Clickhouse_isFirstInsert.set(this, void 0);
        this.formatInsertRows = (rows) => {
            const keysArr = Object.keys(rows[0]);
            const valuesSqlArr = rows.map(row => `(${keysArr.map(key => this.formatInsertValue(row[key])).join(',')})`);
            return { keysArr, valuesSqlFormat: valuesSqlArr.join(',') };
        };
        this.formatInsertValue = (value) => {
            if (Array.isArray(value)) {
                return `[${value.map(this.formatInsertValue).join(',')}]`;
            }
            if (typeof value === 'object') {
                const mapValues = Object.entries(value).map((v) => {
                    return [sqlstring_1.default.escape(v[0]), this.formatInsertValue(v[1])];
                });
                return `map(${mapValues.join(',')})`;
            }
            return sqlstring_1.default.escape(value);
        };
        __classPrivateFieldSet(this, _Clickhouse_isFirstInsert, true, "f");
        __classPrivateFieldSet(this, _Clickhouse_onChunkCb, [], "f");
        __classPrivateFieldSet(this, _Clickhouse_options, options, "f");
        __classPrivateFieldSet(this, _Clickhouse_httpClient, new ClickhouseHttpClient_1.ClickhouseHttpClient({ context, options: options.clickhouseOptions }), "f");
        __classPrivateFieldSet(this, _Clickhouse_cacheManager, this.getCacheManager(__classPrivateFieldGet(this, _Clickhouse_options, "f").cache?.provider ?? 'none'), "f");
        if (__classPrivateFieldGet(this, _Clickhouse_options, "f").debug) {
            Debug_1.debug.setDebugMode(__classPrivateFieldGet(this, _Clickhouse_options, "f").debug.mode);
            Debug_1.debug.excludeDebugProviders(__classPrivateFieldGet(this, _Clickhouse_options, "f").debug.exclude ?? []);
        }
    }
    getCacheManager(provider) {
        const options = {
            chunkTTLSeconds: __classPrivateFieldGet(this, _Clickhouse_options, "f").cache.chunkTTLSeconds,
            chunkExpireTimeSeconds: __classPrivateFieldGet(this, _Clickhouse_options, "f").cache.chunkExpireTimeSeconds,
            chunkSizeLimit: __classPrivateFieldGet(this, _Clickhouse_options, "f").cache.chunkSizeLimit,
            chunkResolverIntervalSeconds: __classPrivateFieldGet(this, _Clickhouse_options, "f").cache.chunkResolverIntervalSeconds,
            chunkResolveType: __classPrivateFieldGet(this, _Clickhouse_options, "f").cache.chunkResolveType,
            useInsert: async (table, rows) => {
                Debug_1.debug.log('hooks.useInsert', { table, rows });
                this.insert(table, rows);
            }
        };
        if (provider === 'redis') {
            return new RedisCacheManager_1.RedisCacheManager(options);
        }
        if (provider === 'nodejs') {
            return new NodeJSCacheManager_1.NodeJSCacheManager(options);
        }
    }
    /**
     *
     * @param query database
     * @param rows [{ value1: 'text' }, {value2: number}]
     *
    */
    async insert(table, rows, options = {}) {
        if (!rows.length) {
            return { inserted: rows.length, data: rows };
        }
        const { keysArr, valuesSqlFormat } = this.formatInsertRows(rows);
        const keys = keysArr.join(',');
        await __classPrivateFieldGet(this, _Clickhouse_httpClient, "f").request({
            params: { query: `INSERT INTO ${table} (${keys}) VALUES` },
            data: valuesSqlFormat,
            requestOptions: options
        });
        return { inserted: rows.length, data: rows };
    }
    /**
     *
     * @param query WITH now() as time SELECT time;
     *
    */
    async query(query, options = {}) {
        const request = `${query} FORMAT ${options.responseFormat ?? __classPrivateFieldGet(this, _Clickhouse_options, "f").defaultResponseFormat}`;
        Debug_1.debug.log('Clickhouse.query', request);
        return __classPrivateFieldGet(this, _Clickhouse_httpClient, "f").request({ data: request });
    }
    useCaching(type, client) {
        if (!__classPrivateFieldGet(this, _Clickhouse_cacheManager, "f")) {
            throw new Error('Cache manager is not initialized!');
        }
        if (type === 'redis') {
            if (!client) {
                throw new Error('Redis client is required');
            }
            __classPrivateFieldSet(this, _Clickhouse_redisClient, client, "f");
        }
        __classPrivateFieldGet(this, _Clickhouse_cacheManager, "f").on('chunk', (chunkId, table, rows) => {
            Debug_1.debug.log('Clickhouse.useCaching', 'received event \'chunk\'', { chunkId, table, rowsCount: rows.length, firstRow: rows[0] });
            __classPrivateFieldGet(this, _Clickhouse_onChunkCb, "f").forEach(cb => cb(chunkId, table, rows));
        });
    }
    onChunk(onChunkCb) {
        __classPrivateFieldGet(this, _Clickhouse_onChunkCb, "f").push(onChunkCb);
    }
    async cache(table, rows) {
        if (!__classPrivateFieldGet(this, _Clickhouse_cacheManager, "f")) {
            throw new Error('CacheClient is not implemented');
        }
        if (__classPrivateFieldGet(this, _Clickhouse_isFirstInsert, "f")) {
            Debug_1.debug.log('Clickhouse.cache', 'Implementing redis cache instance');
            __classPrivateFieldSet(this, _Clickhouse_isFirstInsert, false, "f");
            if (__classPrivateFieldGet(this, _Clickhouse_cacheManager, "f") instanceof RedisCacheManager_1.RedisCacheManager) {
                await __classPrivateFieldGet(this, _Clickhouse_cacheManager, "f").useRedisInstance(__classPrivateFieldGet(this, _Clickhouse_redisClient, "f"));
            }
        }
        const result = await __classPrivateFieldGet(this, _Clickhouse_cacheManager, "f")
            .cache(table, rows.map(row => JSON.stringify(row)));
        return result;
    }
}
exports.Clickhouse = Clickhouse;
_Clickhouse_httpClient = new WeakMap(), _Clickhouse_options = new WeakMap(), _Clickhouse_cacheManager = new WeakMap(), _Clickhouse_onChunkCb = new WeakMap(), _Clickhouse_redisClient = new WeakMap(), _Clickhouse_isFirstInsert = new WeakMap();
