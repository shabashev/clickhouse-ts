/// <reference types="node" />
export declare namespace ClickhouseNamespace {
    type Constructor = {
        url: string;
        port: number;
        user: string;
        password: string;
        database: string;
        ca?: Buffer;
    };
    type Options = {
        debug?: {
            mode: boolean;
            exclude?: ('RedisCacheClientManager.#deleteChunk' | 'RedisCacheClientManager.#checkChunk' | 'RedisCacheClientManager.#createChunk' | 'RedisCacheClientManager.useRedisInstance' | 'RedisCacheClientManager.cache' | 'hooks.useInsert' | 'Clickhouse.insert' | 'Clickhouse.query' | 'Clickhouse.cache' | 'Clickhouse.useCaching' | 'ClickhouseHttpClient.request')[];
        };
        clickhouseOptions?: Record<string, string>;
        cache?: {
            provider: 'redis' | 'nodejs';
            chunkTTLSeconds: number;
            chunkExpireTimeSeconds: number;
            chunkResolverIntervalSeconds: number;
            chunkSizeLimit: number;
            chunkResolveType: 'autoInsert' | 'events';
        };
        defaultResponseFormat: 'JSON' | 'CSV' | 'TSV' | string;
    };
    type InsertRows = Record<string, any>[];
    type InsertOptions = {
        responseFormat?: 'JSON' | 'CSV' | 'TSV' | string;
    } & Record<string, any>;
    type QueryOptions = {
        responseFormat?: 'JSON' | 'CSV' | 'TSV' | string;
    } & Record<string, any>;
}
