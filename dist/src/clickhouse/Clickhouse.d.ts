import { ClickhouseNamespace } from './interface';
import { Redis } from 'ioredis';
export declare class Clickhouse {
    #private;
    constructor(context: ClickhouseNamespace.Constructor, options: ClickhouseNamespace.Options);
    private getCacheManager;
    private formatInsertRows;
    private formatInsertValue;
    /**
     *
     * @param query database
     * @param rows [{ value1: 'text' }, {value2: number}]
     *
    */
    insert(table: string, rows: ClickhouseNamespace.InsertRows, options?: ClickhouseNamespace.InsertOptions): Promise<{
        inserted: number;
        data: ClickhouseNamespace.InsertRows;
    }>;
    /**
     *
     * @param query WITH now() as time SELECT time;
     *
    */
    query(query: string, options?: ClickhouseNamespace.QueryOptions): Promise<{
        headers: Record<string, string>;
        status: number;
        statusText: string;
        data: any;
    }>;
    useCaching(type: 'redis' | 'nodejs', client?: Redis): void;
    onChunk(onChunkCb: (chunkId: string, table: string, rows: ClickhouseNamespace.InsertRows) => void): void;
    cache(table: string, rows: ClickhouseNamespace.InsertRows): Promise<{
        cached: number;
        chunk: string;
    }>;
}
