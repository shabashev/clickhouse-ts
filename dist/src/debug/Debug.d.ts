export declare class Debug {
    #private;
    constructor();
    setDebugMode(mode: boolean): void;
    excludeDebugProviders(providers: string[]): void;
    log(provider: string, ...args: (string | number | Object | Function)[]): void;
}
export declare const debug: Debug;
