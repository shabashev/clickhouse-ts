"use strict";
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var _Debug_isDebugMode, _Debug_providersBlacklist;
Object.defineProperty(exports, "__esModule", { value: true });
exports.debug = exports.Debug = void 0;
class Debug {
    constructor() {
        _Debug_isDebugMode.set(this, void 0);
        _Debug_providersBlacklist.set(this, void 0);
        __classPrivateFieldSet(this, _Debug_isDebugMode, false, "f");
        __classPrivateFieldSet(this, _Debug_providersBlacklist, [], "f");
    }
    setDebugMode(mode) {
        __classPrivateFieldSet(this, _Debug_isDebugMode, mode, "f");
    }
    excludeDebugProviders(providers) {
        __classPrivateFieldGet(this, _Debug_providersBlacklist, "f").push(...providers);
    }
    log(provider, ...args) {
        if (__classPrivateFieldGet(this, _Debug_isDebugMode, "f") &&
            !__classPrivateFieldGet(this, _Debug_providersBlacklist, "f").includes(provider)) {
            console.log(`[Debug][${provider}]`, ...args);
        }
    }
}
exports.Debug = Debug;
_Debug_isDebugMode = new WeakMap(), _Debug_providersBlacklist = new WeakMap();
exports.debug = new Debug();
