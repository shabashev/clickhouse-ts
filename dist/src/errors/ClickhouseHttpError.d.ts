import { AxiosError } from "axios";
export declare class ClickhouseHttpError extends Error {
    status?: number;
    statusText?: string;
    headers: Record<string, any>;
    config?: Record<string, any>;
    constructor(error: AxiosError['response']);
}
