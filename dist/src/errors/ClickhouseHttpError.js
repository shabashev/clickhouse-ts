"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClickhouseHttpError = void 0;
class ClickhouseHttpError extends Error {
    constructor(error) {
        super();
        this.message = error?.data;
        this.status = error?.status;
        this.statusText = error?.statusText;
        this.headers = error?.headers;
        this.config = error?.config;
    }
}
exports.ClickhouseHttpError = ClickhouseHttpError;
