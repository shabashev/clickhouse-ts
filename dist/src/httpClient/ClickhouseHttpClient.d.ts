/// <reference types="node" />
declare namespace ClickhouseHttpClient {
    type Constructor = {
        context: {
            url: string;
            port: number;
            user: string;
            password: string;
            database: string;
            ca?: Buffer;
        };
        options?: Record<string, any>;
    };
    type Request = {
        params?: Record<string, any>;
        data: string;
        requestOptions?: Record<string, string>;
    };
    type Response = {
        headers: Record<string, string>;
        status: number;
        statusText: string;
        data: any;
    };
}
export declare class ClickhouseHttpClient {
    #private;
    constructor({ context, options }: ClickhouseHttpClient.Constructor);
    request({ params, data, requestOptions }: ClickhouseHttpClient.Request): Promise<ClickhouseHttpClient.Response>;
}
export {};
