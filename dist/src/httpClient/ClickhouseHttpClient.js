"use strict";
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _ClickhouseHttpClient_axios, _ClickhouseHttpClient_https, _ClickhouseHttpClient_url, _ClickhouseHttpClient_port, _ClickhouseHttpClient_ca, _ClickhouseHttpClient_password, _ClickhouseHttpClient_user, _ClickhouseHttpClient_database, _ClickhouseHttpClient_options;
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClickhouseHttpClient = void 0;
const axios_1 = __importDefault(require("axios"));
const https_1 = __importDefault(require("https"));
const Debug_1 = require("../debug/Debug");
const ClickhouseHttpError_1 = require("../errors/ClickhouseHttpError");
class ClickhouseHttpClient {
    constructor({ context, options = {} }) {
        // DI
        _ClickhouseHttpClient_axios.set(this, axios_1.default);
        _ClickhouseHttpClient_https.set(this, https_1.default);
        _ClickhouseHttpClient_url.set(this, void 0);
        _ClickhouseHttpClient_port.set(this, void 0);
        _ClickhouseHttpClient_ca.set(this, void 0);
        _ClickhouseHttpClient_password.set(this, void 0);
        _ClickhouseHttpClient_user.set(this, void 0);
        _ClickhouseHttpClient_database.set(this, void 0);
        _ClickhouseHttpClient_options.set(this, void 0);
        __classPrivateFieldSet(this, _ClickhouseHttpClient_url, context.url, "f");
        __classPrivateFieldSet(this, _ClickhouseHttpClient_port, context.port, "f");
        __classPrivateFieldSet(this, _ClickhouseHttpClient_user, context.user, "f");
        __classPrivateFieldSet(this, _ClickhouseHttpClient_password, context.password, "f");
        __classPrivateFieldSet(this, _ClickhouseHttpClient_database, context.database, "f");
        __classPrivateFieldSet(this, _ClickhouseHttpClient_ca, context.ca, "f");
        __classPrivateFieldSet(this, _ClickhouseHttpClient_options, options, "f");
    }
    async request({ params, data = '', requestOptions = {} }) {
        return new Promise((resolve, reject) => {
            const config = {
                maxBodyLength: Infinity,
                method: 'POST',
                url: `${__classPrivateFieldGet(this, _ClickhouseHttpClient_url, "f")}:${__classPrivateFieldGet(this, _ClickhouseHttpClient_port, "f")}`,
                params: new URLSearchParams({
                    ...params,
                    user: __classPrivateFieldGet(this, _ClickhouseHttpClient_user, "f"),
                    password: __classPrivateFieldGet(this, _ClickhouseHttpClient_password, "f"),
                    database: __classPrivateFieldGet(this, _ClickhouseHttpClient_database, "f"),
                    ...__classPrivateFieldGet(this, _ClickhouseHttpClient_options, "f"),
                    ...requestOptions
                }),
                data: data,
                ...__classPrivateFieldGet(this, _ClickhouseHttpClient_ca, "f") && { httpsAgent: new (__classPrivateFieldGet(this, _ClickhouseHttpClient_https, "f").Agent)({ ca: __classPrivateFieldGet(this, _ClickhouseHttpClient_ca, "f") }) }
            };
            Debug_1.debug.log('ClickhouseHttpClient.request', 'Http request', { config });
            __classPrivateFieldGet(this, _ClickhouseHttpClient_axios, "f")
                .request(config)
                .then(response => resolve({
                headers: response.headers,
                data: response.data,
                status: response.status,
                statusText: response.statusText
            }))
                .catch((error) => {
                console.log(error);
                reject(new ClickhouseHttpError_1.ClickhouseHttpError(error.response));
            });
        });
    }
}
exports.ClickhouseHttpClient = ClickhouseHttpClient;
_ClickhouseHttpClient_axios = new WeakMap(), _ClickhouseHttpClient_https = new WeakMap(), _ClickhouseHttpClient_url = new WeakMap(), _ClickhouseHttpClient_port = new WeakMap(), _ClickhouseHttpClient_ca = new WeakMap(), _ClickhouseHttpClient_password = new WeakMap(), _ClickhouseHttpClient_user = new WeakMap(), _ClickhouseHttpClient_database = new WeakMap(), _ClickhouseHttpClient_options = new WeakMap();
