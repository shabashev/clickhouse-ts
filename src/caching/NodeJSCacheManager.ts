import dayjs from "dayjs";
import { Redis } from "ioredis";
import { EventEmitter } from "stream";
import crypto from 'crypto'
import { debug } from "../debug/Debug";

export class NodeJSCacheManager extends EventEmitter {
  #tableChunks: Record<string, Record<string, unknown[]>>;
  #chunkResolver?: NodeJS.Timeout
  readonly #splitter = '-'
  #options: {
    chunkTTLSeconds: number;
    chunkExpireTimeSeconds: number;
    chunkSizeLimit: number;
    chunkResolverIntervalSeconds: number;
    chunkResolveType: 'autoInsert' | 'events';
    useInsert: (table: string, rows: Record<string, any>[]) => Promise<void>;
  };

  constructor(options: {
    chunkTTLSeconds: number,
    chunkExpireTimeSeconds: number,
    chunkSizeLimit: number,
    chunkResolverIntervalSeconds: number,
    chunkResolveType: 'autoInsert' | 'events',
    useInsert: (table: string, rows: Record<string, any>[]) => Promise<void>
  }) {
    super()
    if (options.chunkExpireTimeSeconds <= options.chunkTTLSeconds) {
      throw new Error('chunkExpireTimeSeconds must be greater then chunkTTLSeconds')
    }
    this.#tableChunks = {}

    this.#options = options

    const intervalTime = 1000 * this.#options.chunkResolverIntervalSeconds
    this.#chunkResolver = setInterval(async () => await this.#checkChunks(), intervalTime)
  }

  readonly #checkChunks = async () => {
    debug.log('tableChunks_profiling', { tableChunks: this.#tableChunks })

    for await (const [table, chunkIds] of Object.entries(this.#tableChunks)) {
      for await (const chunkId of Object.keys(chunkIds)) {
        const chunkLen = this.#tableChunks[table][chunkId].length
        debug.log(`chunk.${table}.${chunkId}`, { chunkLen })
      }
    }

    const now = dayjs()
    for await (const [table, chunkNamespaces] of Object.entries(this.#tableChunks)) {
      for (const chunkNamespace of Object.keys(chunkNamespaces)) {

        debug.log(
          'RedisCacheClientManager.#checkChunk',
          { table, chunkNamespace }
        )

        const [_chunk_, _table, _id, _strExpiresAtUnix] = chunkNamespace.split(this.#splitter)
        const expiresAt = Number(_strExpiresAtUnix)

        // const chunkNamespaceTTL = await this.#instance!.ttl(chunkNamespace)

        // if (chunkNamespaceTTL < 0) {
        //   await this.#instance!.expireat(
        //     chunkNamespace,
        //     now.add(this.#options.chunkExpireTimeSeconds ?? 86400, 's').unix()
        //   )
        // }

        // const chunkFull = await this.#instance!.llen(chunkNamespace) >= this.#options.chunkSizeLimit
        const chunkTooOld = now.unix() > expiresAt

        if (chunkTooOld) {
          await this.#resolveChunk(chunkNamespace, table)
        }
      }
    }
  }

  readonly #resolveChunk = async (chunkId: string, table: string) => {
    const raw = this.#tableChunks[table][chunkId]
    const rows = raw.map(str => JSON.parse(str as string))

    if (this.#options.chunkResolveType === 'autoInsert') {
      await this.#options.useInsert(table, rows)
    } else if (this.#options.chunkResolveType === 'events') {
      this.emit('chunk', chunkId, table, [...rows])
    } else {
      throw new Error('resolveType is not correct!')
    }

    this.#deleteChunk(table, chunkId)
  }

  readonly #createChunk = (table: string) => {
    const now = dayjs()

    // if (!this.#tableChunks[table]) {
    //   this.#tableChunks[table] = []
    // }

    const id = crypto
      .createHash('md5')
      .update((Math.random() * 9e5).toString())
      .digest('hex')

    const ttl = now.add(this.#options.chunkTTLSeconds, 'second').unix()

    const newChunk = ['chunk', table, id, ttl].join(this.#splitter)

    this.#tableChunks[table][newChunk] = []

    debug.log(
      'RedisCacheClientManager.#createChunk',
      { table, newChunk, tableChunks: this.#tableChunks }
    )

    return newChunk
  }

  readonly #deleteChunk = (table: string, chunk: string) => {
    // if (!this.#tableChunks[table]) {
      // this.#tableChunks[table] = []
    // }

    // this.#tableChunks[table] = this.#tableChunks[table].filter(tableChunk => tableChunk !== chunk)

    delete this.#tableChunks[table][chunk]

    debug.log(
      'RedisCacheClientManager.#deleteChunk',
      { table, chunk, tableChunks: this.#tableChunks }
    )
  }

  readonly #getChunk = (table: string) => {
    const now = dayjs()
    if (!this.#tableChunks[table]) {
      this.#tableChunks[table] = {}
    }
    const hasChunk = Boolean(Object.values(this.#tableChunks[table]))

    if (hasChunk) {
      const chunk = Object.keys(this.#tableChunks[table]).find(chunk => {
        const [_chunk, _table, _id, strExpiresAtUnix] = chunk.split(this.#splitter)
        const expiresAt = Number(strExpiresAtUnix)

        if (now.unix() < expiresAt) {
          return true
        }
      })

      return chunk ?? this.#createChunk(table)
    } else {
      return this.#createChunk(table)
    }
  }

  public async cache(table: string, items: string[]) {
    let chunkCandidate = this.#getChunk(table)
    while (
      this.#tableChunks[table][chunkCandidate].length >= this.#options.chunkSizeLimit
    ) {
      await this.#resolveChunk(chunkCandidate, table)
      chunkCandidate = this.#getChunk(table)
    }

    this.#tableChunks[table][chunkCandidate].push(...items)

    // const cached = await this.#instance!.rpush(chunkCandidate, ...items)

    debug.log(
      'RedisCacheClientManager.cache',
      {
        cached: items.length,
        chunk: chunkCandidate
      }
    )

    return {
      cached: items.length,
      chunk: chunkCandidate,
    }
  }
}
