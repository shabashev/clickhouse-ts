import dayjs from "dayjs";
import { Redis } from "ioredis";
import { EventEmitter } from "stream";
import crypto from 'crypto'
import { debug } from "../debug/Debug";

export class RedisCacheManager extends EventEmitter {
  #tableChunks: Record<string, string[]>;
  #instance?: Redis;
  #chunkResolver?: NodeJS.Timeout
  readonly #splitter = '-'
  #options: {
    chunkTTLSeconds: number;
    chunkExpireTimeSeconds: number;
    chunkSizeLimit: number;
    chunkResolverIntervalSeconds: number;
    chunkResolveType: 'autoInsert' | 'events';
    useInsert: (table: string, rows: Record<string, any>[]) => Promise<void>;
  };

  constructor(options: {
    chunkTTLSeconds: number,
    chunkExpireTimeSeconds: number,
    chunkSizeLimit: number,
    chunkResolverIntervalSeconds: number,
    chunkResolveType: 'autoInsert' | 'events',
    useInsert: (table: string, rows: Record<string, any>[]) => Promise<void>
  }) {
    super() 
    if (options.chunkExpireTimeSeconds <= options.chunkTTLSeconds) {
      throw new Error('chunkExpireTimeSeconds must be greater then chunkTTLSeconds')
    } 
    this.#tableChunks = {}

    this.#options = options
  }

  readonly #checkChunks = async () => {
    this.checkInstance()

    debug.log('tableChunks_profiling', { tableChunks: this.#tableChunks })

    for await (const [table, chunkIds] of Object.entries(this.#tableChunks)) {
      for await (const chunkId of chunkIds) {
        const chunkLen = await this.#instance!.llen(chunkId)
        debug.log(`chunk.${table}.${chunkId}`, { chunkLen })
      }
    }

    const now = dayjs()
    for await (const [table, chunkNamespaces] of Object.entries(this.#tableChunks)) {
      for (const chunkNamespace of chunkNamespaces) {
        
        debug.log(
          'RedisCacheClientManager.#checkChunk',
          { table, chunkNamespace }
        )

        const [_chunk_, _table, _id, _strExpiresAtUnix] = chunkNamespace.split(this.#splitter)
        const expiresAt = Number(_strExpiresAtUnix)

        const chunkNamespaceTTL = await this.#instance!.ttl(chunkNamespace)

        if (chunkNamespaceTTL < 0) {
          await this.#instance!.expireat(
            chunkNamespace,
            now.add(this.#options.chunkExpireTimeSeconds ?? 86400, 's').unix()
          )
        }

        // const chunkFull = await this.#instance!.llen(chunkNamespace) >= this.#options.chunkSizeLimit
        const chunkTooOld = now.unix() > expiresAt

        if (chunkTooOld) {
          await this.#resolveChunk(chunkNamespace, table)
        }
      }
    }
  }

  readonly #resolveChunk = async (chunkId: string, table: string) => {
    const raw = await this.#instance!.lrange(chunkId, 0, -1)
    const rows = raw.map(str => JSON.parse(str))

    if (this.#options.chunkResolveType === 'autoInsert') {
      await this.#options.useInsert(table, rows)
    } else if (this.#options.chunkResolveType === 'events') {
      this.emit('chunk', chunkId, table, rows)
    } else {
      throw new Error('resolveType is not correct!')
    }

    await this.#deleteChunk(table, chunkId)
  }

  readonly checkInstance = () => {
    if (!this.#instance) {
      throw new Error('Redis instance is not implemented!')
    }
  }

  readonly #createChunk = (table: string) => {
    const now = dayjs()

    if (!this.#tableChunks[table]) {
      this.#tableChunks[table] = []
    }
    
    const id = crypto
      .createHash('md5')
      .update((Math.random() * 9e5).toString())
      .digest('hex')

    const ttl = now.add(this.#options.chunkTTLSeconds, 'second').unix()
    
    const newChunk = ['chunk', table, id, ttl].join(this.#splitter)
    
    this.#tableChunks[table].push(newChunk)

    debug.log(
      'RedisCacheClientManager.#createChunk',
      { table, newChunk, tableChunks: this.#tableChunks }
    )

    return newChunk
  }

  readonly #deleteChunk = async (table: string, chunk: string) => {
    if (!this.#tableChunks[table]) {
      this.#tableChunks[table] = []
    }

    this.#tableChunks[table] = this.#tableChunks[table].filter(tableChunk => tableChunk !== chunk)
    await this.#instance!.del(chunk)

    debug.log(
      'RedisCacheClientManager.#deleteChunk',
      { table, chunk, tableChunks: this.#tableChunks }
    )
  }

  readonly #getChunk = (table: string) => {
    const now = dayjs()
    const hasChunk = Boolean(this.#tableChunks[table]?.length)

    if (hasChunk) {
      const chunk = this.#tableChunks[table].find(chunk => {
        const [_chunk, _table, _id, strExpiresAtUnix] = chunk.split(this.#splitter)
        const expiresAt = Number(strExpiresAtUnix)
        
        if (now.unix() < expiresAt) {
          return true
        }
      })

      return chunk ?? this.#createChunk(table)
    } else {
      return this.#createChunk(table)
    }
  }

  public async useRedisInstance(instance: Redis) {
    if (this.#instance) {
      return
    }

    const intervalTime = 1000 * this.#options.chunkResolverIntervalSeconds

    this.#instance = instance

    const cachedChunkTables = await this.#instance.keys(`chunk${this.#splitter}*`)

    debug.log('RedisCacheClientManager.useRedisInstance', { cachedChunkTables })

    for await (const chunkTable of cachedChunkTables) {
      const [_, table] = chunkTable.split(this.#splitter)

      if (!this.#tableChunks[table]) {
        this.#tableChunks[table] = []
      }

      this.#tableChunks[table].push(chunkTable)
    }

    this.#chunkResolver = setInterval(async () => await this.#checkChunks(), intervalTime)
  }

  public async cache(table: string, items: string[]) {
    this.checkInstance()

    let chunkCandidate = this.#getChunk(table)
    while (
      await this.#instance!.llen(chunkCandidate) >= this.#options.chunkSizeLimit
    ) {
      await this.#resolveChunk(chunkCandidate, table)
      chunkCandidate = this.#getChunk(table)
    }

    const cached = await this.#instance!.rpush(chunkCandidate, ...items)

    debug.log(
      'RedisCacheClientManager.cache',
      { cached, chunk: chunkCandidate }
    )

    return {
      cached,
      chunk: chunkCandidate,
    }
  }
}