export class Debug {
  #isDebugMode: boolean
  #providersBlacklist: string[]

  constructor() {
    this.#isDebugMode = false
    this.#providersBlacklist = []
  }

  public setDebugMode(mode: boolean) {
    this.#isDebugMode = mode
  }

  public excludeDebugProviders(providers: string[]) {
    this.#providersBlacklist.push(...providers)
  }

  public log(provider: string, ...args: (string | number | Object | Function)[]) {
    if (
      this.#isDebugMode &&
      !this.#providersBlacklist.includes(provider)
    ) {
      console.log(`[Debug][${provider}]`, ...args)
    }
  }
}

export const debug = new Debug()