import { Redis } from 'ioredis'

export namespace ClickhouseNamespace {
  export type Constructor = {
    url: string,
    port: number,
    user: string,
    password: string,
    database: string,
    ca?: Buffer
  }

  export type Options = {
    debug?: {
      mode: boolean,
      exclude?: (
        'RedisCacheClientManager.#deleteChunk' |
        'RedisCacheClientManager.#checkChunk' |
        'RedisCacheClientManager.#createChunk' |
        'RedisCacheClientManager.useRedisInstance' |
        'RedisCacheClientManager.cache' |
        'hooks.useInsert' |
        'Clickhouse.insert' |
        'Clickhouse.query' |
        'Clickhouse.cache' |
        'Clickhouse.useCaching' |
        'ClickhouseHttpClient.request'
      )[]
    },
    clickhouseOptions?: Record<string, string>,
    cache?: {
      provider: 'redis' | 'nodejs',
      chunkTTLSeconds: number,
      chunkExpireTimeSeconds: number,
      chunkResolverIntervalSeconds: number,
      chunkSizeLimit: number,
      chunkResolveType: 'autoInsert' | 'events',
    }
    defaultResponseFormat: 'JSON' | 'CSV' | 'TSV' | string
  }

  export type InsertRows = Record<string, any>[]
  export type InsertOptions = {
    responseFormat?: 'JSON' | 'CSV' | 'TSV' | string
  } & Record<string, any>

  export type QueryOptions = {
    responseFormat?: 'JSON' | 'CSV' | 'TSV' | string
  } & Record<string, any>
}
