import { ClickhouseHttpClient } from '../httpClient/ClickhouseHttpClient'
import sqlstring from 'sqlstring'
import { ClickhouseNamespace } from './interface'
import { Redis } from 'ioredis'
import { RedisCacheManager } from '../caching/RedisCacheManager'
import { debug } from '../debug/Debug'
import { NodeJSCacheManager } from '../caching/NodeJSCacheManager'



export class Clickhouse {
  // DI
  readonly #httpClient: ClickhouseHttpClient
  readonly #options: ClickhouseNamespace.Options
  readonly #cacheManager?: RedisCacheManager | NodeJSCacheManager
  #onChunkCb: ((chunkId: string, table: string, rows: ClickhouseNamespace.InsertRows) => void)[]
  #redisClient?: Redis
  #isFirstInsert: boolean

  constructor(
    context: ClickhouseNamespace.Constructor,
    options: ClickhouseNamespace.Options
  ) {
    this.#isFirstInsert = true
    this.#onChunkCb = []
    this.#options = options
    this.#httpClient = new ClickhouseHttpClient({ context, options: options.clickhouseOptions })

    this.#cacheManager = this.getCacheManager(this.#options.cache?.provider ?? 'none')

      if (this.#options.debug) {
        debug.setDebugMode(this.#options.debug.mode)
        debug.excludeDebugProviders(this.#options.debug.exclude ?? [])
      }
  }

  private getCacheManager(provider: 'redis' | 'nodejs' | 'none') {
    const options = {
      chunkTTLSeconds: this.#options.cache!.chunkTTLSeconds,
      chunkExpireTimeSeconds: this.#options.cache!.chunkExpireTimeSeconds,
      chunkSizeLimit: this.#options.cache!.chunkSizeLimit,
      chunkResolverIntervalSeconds: this.#options.cache!.chunkResolverIntervalSeconds,
      chunkResolveType: this.#options.cache!.chunkResolveType,
      useInsert: async (table: string, rows: ClickhouseNamespace.InsertRows) => {
        debug.log('hooks.useInsert', { table, rows })
        this.insert(table, rows)
      }
    }

    if (provider === 'redis') {
      return new RedisCacheManager(options)
    }

    if (provider === 'nodejs') {
      return new NodeJSCacheManager(options)
    }
  }

  private formatInsertRows = (rows: ClickhouseNamespace.InsertRows) => {
    const keysArr = Object.keys(rows[0])
    const valuesSqlArr = rows.map(row => `(${keysArr.map(key => this.formatInsertValue(row[key])).join(',')})`)
    return { keysArr, valuesSqlFormat: valuesSqlArr.join(',') }
  }

  private formatInsertValue = (value: any): string => {
    if (Array.isArray(value)) {
      return `[${value.map(this.formatInsertValue).join(',')}]`
    }

    if (typeof value === 'object') {
      const mapValues = Object.entries(value).map((v) => {
        return [sqlstring.escape(v[0]), this.formatInsertValue(v[1])]
      })
      return `map(${mapValues.join(',')})`
    }

    return sqlstring.escape(value)
  }

  /**
   *
   * @param query database
   * @param rows [{ value1: 'text' }, {value2: number}]
   *
  */
  public async insert(
    table: string,
    rows: ClickhouseNamespace.InsertRows,
    options: ClickhouseNamespace.InsertOptions = {}
  ) {
    if (!rows.length) {
      return { inserted: rows.length, data: rows }
    }

    const { keysArr, valuesSqlFormat } = this.formatInsertRows(rows)
    const keys = keysArr.join(',')
    await this.#httpClient.request({
      params: { query: `INSERT INTO ${table} (${keys}) VALUES` },
      data: valuesSqlFormat,
      requestOptions: options
    })

    return { inserted: rows.length, data: rows }
  }

  /**
   *
   * @param query WITH now() as time SELECT time;
   *
  */
  public async query(
    query: string,
    options: ClickhouseNamespace.QueryOptions = {}
  ) {
    const request = `${query} FORMAT ${options.responseFormat ?? this.#options.defaultResponseFormat}`

    debug.log('Clickhouse.query', request)

    return this.#httpClient.request({ data: request })
  }

  public useCaching(type: 'redis' | 'nodejs', client?: Redis) {
    if (!this.#cacheManager) {
      throw new Error('Cache manager is not initialized!')
    }

    if (type === 'redis') {
      if (!client) {
        throw new Error('Redis client is required')
      }
      this.#redisClient = client
    }

    this.#cacheManager.on('chunk', (
      chunkId: string,
      table: string,
      rows: Record<string, any>[]
    ) => {
      debug.log(
        'Clickhouse.useCaching',
        'received event \'chunk\'',
        { chunkId, table, rowsCount: rows.length, firstRow: rows[0] }
      )
      this.#onChunkCb.forEach(cb => cb(chunkId, table, rows))
    })
  }

  public onChunk (
    onChunkCb: (chunkId: string, table: string, rows: ClickhouseNamespace.InsertRows) => void
  ) {
    this.#onChunkCb.push(onChunkCb)
  }

  public async cache(
    table: string,
    rows: ClickhouseNamespace.InsertRows
  ) {
    if (!this.#cacheManager) {
      throw new Error('CacheClient is not implemented')
    }

    if (this.#isFirstInsert) {
      debug.log('Clickhouse.cache', 'Implementing redis cache instance')

      this.#isFirstInsert = false
      if (this.#cacheManager instanceof RedisCacheManager) {
        await this.#cacheManager.useRedisInstance(this.#redisClient!)
      }
    }

    const result = await this.#cacheManager
      .cache(
        table,
        rows.map(row => JSON.stringify(row))
      )
    return result
  }
}
